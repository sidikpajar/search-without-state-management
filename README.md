## Getting Started

First, run the development server:

```bash
npm run start
# or
yarn start
```

Make sure you have .env file on your root folder.
.env file use for environtment of the website.


Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

This Web Apps using Atomic Design Patter, open [https://atomicdesign.bradfrost.com/chapter-2/] (Atomic Design Pattern) to see how the methodology works.

API Instance:
```
src/api/index.ts
```

this is structure of atomic design pattern on this project
```
src/component/atoms
src/component/molecules
src/component/pages
src/component/template
```

