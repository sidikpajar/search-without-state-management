import axios, { AxiosInstance } from 'axios';

interface BaseConfig {
  baseURL: string | undefined;
  headers: {
    'Content-Type': string;
    Accept: string;
    'X-GitHub-Api-Version': string;
    Authorization: string;
  };
  /* other custom settings */
  validateStatus: () => boolean;
  timeout: number;
}

const baseConfig: BaseConfig = {
  baseURL: process.env.REACT_APP_API_HOST, // development
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/vnd.github+json',
    'X-GitHub-Api-Version': '2022-11-28',
    Authorization: `Bearer ${process.env.REACT_APP_PERSONAL_ACCESS_TOKEN}`,
  },
  /* other custom settings */
  validateStatus: () => true,
  timeout: 10000,
};

const api: AxiosInstance = axios.create(baseConfig);

export default api;
