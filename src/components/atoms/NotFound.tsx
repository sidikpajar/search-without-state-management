
interface NotFoundProps {
  label: string,
}

export default function NotFound(props: NotFoundProps) {
  const {
    label,
  } = props;
  return (
    <p className='text-red-600 text-sm'>{label}</p>
  )
}
