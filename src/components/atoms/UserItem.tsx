import React, { useState } from 'react'
import {BsChevronDown} from 'react-icons/bs'
import api from 'src/api';
import { Repos } from 'src/types/Repos';
import {FaStar} from 'react-icons/fa'
interface UserItemProps {
  key: number,
  label: string
}
interface ConditionProps {
  show: boolean,
  loading: boolean
}
function UserItem(props: UserItemProps) {
  const {
    key,
    label,
  } = props;
  const [repos, setRepos] = useState<Repos[]>([]);
  const [condition, setCondition] = useState<ConditionProps>({
    show: false,
    loading: false
  })

  const getRepos = async (search: string) => {
    try {
      setCondition({ ...condition, loading: true });
      const res = await api.get(`/users/${search}/repos`);
      if (res?.status === 200) {
        setCondition({ show: true, loading: false });
        setRepos(res.data);
      } else {
        setCondition({ show: true, loading: false });
        setRepos([]);
      }
    } catch (err) {
      setCondition({ ...condition, loading: false });
      setRepos([]);
    }
  }

  return (
    <>
      <div key={key} className='p-3 w-full bg-grey cursor-pointer items-center flex flex-row justify-between' onClick={() => getRepos(label)}>
        <div>{label}</div> <BsChevronDown />
      </div>
      {condition.loading ? (
            <p>Loading...</p>
          ) : condition.show ? (
            repos.length > 0 ? (
              repos.map((item, index) => (
                <div
                  key={index}
                  className="p-3 ml-4 w-full bg-grey2 gap-2 cursor-pointer items-left flex flex-col justify-between"
                >
                  <div className='font-bold flex flex-row justify-between items-center gap-4'>
                    <div className='line-clamp-1'>{item.name}</div>
                    <div className='text-sm flex flex-row items-center gap-1'>{item?.stargazers_count} <FaStar /> </div>
                  </div> 
                  {
                    item?.description && <div className='text-sm'>{item.description}</div> 
                  }
                </div>
              ))
            ) : (
              <p className="text-red-600 text-sm">Data Repository Not Found.</p>
            )
          ) : null}
    </>
   
  )
}

export default UserItem