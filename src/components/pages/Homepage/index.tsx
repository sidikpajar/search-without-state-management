import { useEffect, useState } from 'react'
import api from 'src/api';
import { Users } from 'src/types/Users';
import UserItem from 'src/components/atoms/UserItem';
import NotFound from 'src/components/atoms/NotFound';
interface SearchProps {
  before: string;
  after: string;
}

function Homepage() {
  const [data, setData] = useState<Users>();
  const [loading, setLoading] = useState<Boolean>(false);
  const [search, setSearch] = useState<SearchProps>({
    before: '',
    after: '',
  });

  const fetch = async (search: string) => {
    try {
      setLoading(true)
      const res = await api.get(`/search/users?q=${search}`);
      if (res?.status === 200) {
        setLoading(false)
        setData(res.data);
      } else {
        setLoading(false)
        setData(res.data);
      }
    } catch (err) {
      setLoading(false)
    }
  }

  useEffect(() => {
    fetch(search?.after)
  }, [search?.after]);


  const handleChange = (e: any) => {
    setSearch({ ...search, before: e.target.value });
  };

  return (
    <div className='my-4'>
      <div className='w-9/12 sm:w-3/12 mx-auto flex flex-col gap-4'>
        <input
          type="text"
          value={search.before}
          onChange={handleChange}
          placeholder='Search by username'
          className='w-full border rounded p-3 focus:outline-none focus:border-grey-500 bg-grey'
        />
        <div onClick={() => {
          fetch(search?.before)
          setSearch({...search, after: search?.before})
        }}
          className='text-center bg-primary hover:bg-primaryHover text-white p-3 rounded w-full cursor-pointer border-grey-500'>
          Search
        </div>
        <div className='text-sm'>
          Showing users for "{search?.after}"
        </div>
        {loading ? (
          <p>Loading...</p> // Display a loading
        ) : (
          <div className='flex flex-col gap-4'>
           {(data?.items?.length ?? 0) > 0 ? (
              data?.items?.map((item, index) => (
                <UserItem key={index} label={item?.login}/>
              ))
            ) : (
              <NotFound label="Data Username Not Found."/>
            )}
          </div>
        )}
      
      </div>
    </div>
  )
}

export default Homepage