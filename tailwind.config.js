const colors = require('tailwindcss/colors');
module.exports = {
  corePlugins: {
    preflight: false,
  },
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}"
  ],
  theme: {
    colors:{
      primary: '#509AD6',
      primaryHover: '#2B82C9',
      grey: '#F2F2F2',
      grey2: '#E0E0E0',
      ...colors
    },
    extend: {},
  },
  plugins: [],
}